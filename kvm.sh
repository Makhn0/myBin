#!/bin/bash
export QEMU_AUDIO_DRV=pa
export QEMU_PA_SOURCE=input
export QEMU_PA_SINK=alsa_output.pci-0000_04_01.0.analog-stero.monitor


#drives="sda sda1 sda2 sda3 sda4 sdb sdb1 sdb2 sdb3 sde sde1"
drives="sda sda1 sda2 sda3 sda4 sdb"
drive_flagsa=""
i=0
for d in $drives
do
    drives_flags+="-drive file=/dev/${d},index=${i},media=disk,driver=raw "
    i=$((i+1))
done


sudo qemu-system-x86_64 \
     --enable-kvm -machine q35,accel=kvm -device intel-iommu \
     -cpu host,hv_relaxed,hv_spinlocks=0x1ff,hv_vapic,hv_time \
     -m 12G -smp 12 \
     -display sdl \
     -vga std \
     -device intel-hda \
     -usb -device usb-tablet \
     -audiodev pa,id=pa1,server=/run/user/1000/pulse/native \
     -device ich9-intel-hda -device hda-output,audiodev=pa1 \
      -drive file=/usr/share/ovmf/x64/OVMF_CODE.fd,if=pflash,format=raw,unit=0,readonly=on \
     ${drives_flags} \
     -runas eric


      #-cdrom /opt/UefiShell.iso \
      #-drive file=$HOME/.config/qemu-windows.nvram,if=pflash,format=raw,unit=1 \
     #-device intel-iommu \
