#!/bin/sh
PIC_DIR="$HOME/pix/bg/pngs"
NEW_PIC=$(nsxiv_pic $PIC_DIR) 
LOCK_CONF=$HOME/.config/hypr/hyprlock.conf
sed -i --follow-symlinks  "s@^path=.*\$@path=$NEW_PIC@g" $LOCK_CONF 
