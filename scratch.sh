#!/bin/bash
# $1 title
# $2 main program
# $3 list of prereq programs like mpd 
for i in ${3}; do
         if [[ -z $(pgrep ${i} ) ]]; then
            $i &
         else
	           echo "found $i"
         fi
done

# if it finds it runs the program ${2}
# else quits
# f=$(pgrep -fc "scratch.sh ${1} ${2} ${3}")
f=$(pgrep -fc "${2}")

if [[  "$f" == "1" ]]; then
    exec ${2} &
    sleep .23
fi
echo "${2}" >> ~/${1}d
echo $f >>~/${1}d
f="$(i3 "[title=\"${1}\"] scratchpad show")"
echo "$f"
[[ "$f" =~ "{\"success\":false}" ]] && i3 "[title=\"${1}\"] focus"
