#!/usr/bin/env sh

export BEMENU_OPTS="-i -l 10 --fn 'Hack Nerd Font Regular 14 -c"
export BEMENU_BACKEND=wayland
export BEMENU_SCALE=2

/usr/bin/pinentry-bemenu
