#!/usr/bin/env sh

if [[ $WAYLAND_DISPLAY ]];then
   CFILE=~/.config/wob/wob.ini
   XOB=wob
else
   CFILE=~/.config/xob/styles.cfg
   XOB=xob
   STYLEOPT="-s bw-$(hostname)"
fi
XOBFIFO=/tmp/${XOB}pipe

start_xob(){
#    echo $XOB $CFILE $STYLEOPT $XOBFIFO
#    echo a
    [ -p $XOBFIFO ]|| mkfifo -v $XOBFIFO
#    echo b
    tail -f $XOBFIFO | $XOB -c $CFILE $STYLEOPT  &
#    echo c
    }

case $1 in
    -r) killall -v $XOB
        start_xob
        ;;
    -s) start_xob ;;
    -k) killall xob;;
    *) start_xob;;
esac
