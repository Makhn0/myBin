#!/bin/sh
SERVICE=syncthing
SYSTEMD_COMMAND="systemctl --user"
ICON=$HOME/pix/syncthing-small.png
#DELAY=1000 #deprecated,
#COLORARG=0
#[ -z "$NOTIFY" ] && NOTIFY=notify-send
NOTIFY="notify-send"
echo $ICON

notify_sync () {
	$NOTIFY -i $ICON $1 -a sync -r 542
}
stop_sync ()
{
    notify_sync "stopping..."
$SYSTEMD_COMMAND stop $SERVICE && \
	notify_sync "syncthing stopped" || notify_sync  "failed to stop"
}
start_sync (){
$SYSTEMD_COMMAND --user start $SERVICE && \
	notify_sync "synchthing started" || notify_sync "failed to start"
}
pgrep syncthing && stop_sync || start_sync 
