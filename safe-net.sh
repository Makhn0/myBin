#!/bin/zsh
# regex check doesn't seem to work with /bin/sh
#NEWIP=$HOME/.mypubip.$(/bin/date +%F)
#[ ! -e $NEWIP ] && curl ifconfig.me > $NEWIP 
NEWIP=$(curl ifconfig.me)
#echo $NEWIP
if [[ $NEWIP =~ ".*:.*:.*:.*:.*:.*" ]]; then
	OLDIP=$HOME/.mypubipv6
else
	OLDIP=$HOME/.mypubip
fi
#echo echo oldy= $OLDIP
test  "$(cat $OLDIP)" = "$(curl ifconfig.me)" \
      -o "$(nmcli device | awk '/^wlo1/ {printf $3 " " ; print $4}')" = "connected eduroam" \
      -o "$(nmcli device | awk '/^wlo1/ {printf $3 " " $4 " "; print $5}')" = "connected Jeanie Google"  \
      -o "$(nmcli device | awk '/^wlo1/ {printf $3 " " $4}')" = "connected FunnyWifiName" 
