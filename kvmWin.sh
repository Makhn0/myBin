#!/bin/bash
export QEMU_AUDIO_DRV=pa
export QEMU_PA_SOURCE=input
export QEMU_PA_SINK=alsa_output.pci-0000_04_01.0.analog-stero.monitor

qemu-system-x86_64 \
     --enable-kvm -machine q35,accel=kvm -device intel-iommu \
     -cpu host -m 12G -smp 8 \
     -display sdl \
     -vga std \
     -usb -device usb-tablet \
     -drive file=/dev/sda,index=0,media=disk,driver=raw \
     #-drive file=/dev/sdd,index=2,media=disk,driver=raw \
     #-drive file=/dev/sdc,index=1,media=disk,driver=raw

#-device virtio-net,netdev=vmnic \
     # -cdrom /opt/UefiShell.iso

     # -drive file=/usr/share/ovmf/x64/OVMF_CODE.fd,if=pflash,format=raw,unit=0,readonly=on \
         # -drive file=$HOME/.config/qemu-windows.nvram,if=pflash,format=raw,unit=1 \
    # -netdev user,id=vmnic,smb=/tmp \
    # -device intel-iommu \
