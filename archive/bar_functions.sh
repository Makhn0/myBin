#!/bin/sh
music(){
    song=$(mpc 2>&1|sed -n '1,1p')

    [[ $song =~ "MPD error:" ]] && printf "Without music, life would be a mistake -Nietzche" && exit
    if [ ! last_song ]; then
        last_song=${song}
        export song
    fi
    pause="$(mpc | awk 'FNR==2{print $1}' )"
    if [[ $pause == "[paused]" ]]; then
        pause=⏸
    else
        pause=⯈
    fi
    volume=$(mpc | grep -e "^volume:.*%.*$" | sed -e "s/[^0-9]*//g")

    # spinal tap block
    if [[ $volume -eq "100" ]]; then
        volume=11
    elif [[ $volume -gt 95 ]] ; then
        volume=10
    else
        volume=$((volume / 10 ))
    fi
    ([[  $(mpc |sed -n '1,1p') =~ "volume:" ]] && [[ $song ]] )||  printf "%s" "${song}${prog} ${pause} ♪${volume}"
}

vpn_icon(){
    if [[ $(curl --retry 1 --connect-timeout 2 ifconfig.me 2>/dev/null) == $(cat .mypubip) ]]; then
        printf "👁"
    else
        printf "🛡"
    fi
    }
 volume(){

     #for lemonbar

	   volStatus=$(amixer get Master | tail -n 1 | cut -d '[' -f 4 | sed 's/].*//g')
     if [[ ! $volStatus ]]; then
	       volStatus=$(amixer get Master | tail -n 1 | cut -d '[' -f 3 | sed 's/].*//g')
     fi
	   volLevel=$(amixer get Master | tail -n 1 | cut -d '[' -f 2 | sed 's/%.*//g')

	   # is alsa muted or not muted
	   if [ "$volStatus" == "on" ] ;
	   then
		     #echo "%{Fyellowgreen} $volLevel %{F-}"
		     echo -e "%{F#9ACD32}🔊${volLevel}%%{F-}"
	   else
		     # If it is muted, make the font red
		     #echo "%{Findianred} $volLevel %{F-}"
		     echo -e "%{F#CD5C5C}🔇${volLevel}%%{F-}"
	   fi
     label="🔊"
 }

 timetoclock(){
     clock=🕐
     u=1F5
     hour=${1%%:*}
     minute=${1##*:}
     case $hour in
         [1][3-9])
         ;&
         [2][0-3])
             hour=$((hour -12))
             ;;
         [0][0])
             hour=12
             ;;
     esac
     case $minute in
         [012][0-9])
             u=${u}5$(printf "%x" $(( hour - 1)))
             ;;
         [345][0-9])
             u=${u}$(printf "%x" $(( 16*5 +12 +hour - 1)))
             ;;
     esac
     perl -CO -E "say \"\N{U+$u}\""
     # printf and echo -e not working on laptop for some reason
     #printf "\U$u"
 }

 myclock()
 {
     datestring="$(date "+%m-%d")"
     time=$(date "+%H:%M")
     echo -e "📅${datestring} $(timetoclock $time)${time}"
 }
