#!/bin/bash
s=${1}
if [[ ${1} =~ ^[1-4]$ ]]; then
	if [[ ${2} =~ ^[1-9][0-9]?$ ]]; then
		e=${2}		
	else
		echo "use $0 series#(1-4)  episode#"
		exit 2
	fi
elif [[ ${1} == next ]]; then
	fstart=$(cat ~/monty) 	
	echo fstart=$fstart

	s=$(grep -oP -e "(?<=MPFC S0)\d"<<<$fstart)

	e=$(grep -oP -e "(?<=MPFC S0\dE)[0-9][0-9]"<<<$fstart)
	e=$(( 10#$e ))

	f="/e/media/tv/Monty Python's Flying Circus - The Complete Series/Series ${s}"
	n=$(ls "$f"| wc -l)

	if [[ $e -ge $n ]]; then
		#season increment
		echo "next season"
		s=$(( s+1 ))	
		e=1
	else 
		e=$(( e+1 ))
	fi
	if [[ $s -eq 5 ]]; then
		echo "end of the line partner"
		exit 4
	fi	
else
	echo "use $0 series#(1-4)  episode#"
	exit 1
fi

f="/e/media/tv/Monty Python's Flying Circus - The Complete Series/Series ${s}"
n=$(ls "$f"| wc -l)


if [[ $e -lt 10 ]]; then
	fstart="MPFC S0${s}E0${e}"
else
	fstart="MPFC S0${s}E${e}"
fi

fname=$(ls "$f" | grep -oP -e "${fstart}.*$")

echo s=$s
echo e=$e
echo f=$f
echo n=$n
echo fname=$fname

if [[ -z $fname  ]]; then 
	echo "I'm afraid this season only has $n episodes"
	exit 3
fi


echo opening: $fname
cvlc "${f}/${fname}"
echo $fstart > ~/monty 
exit 0
