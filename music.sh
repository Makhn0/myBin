#!/bin/bash
# uncomment if used with i3
# i3 "[title=\"^music$\"] scratchpad show"

if [[ "$(pgrep -fc "st -T MUSICPAD -e music.sh" 2> /dev/null )" -gt 1 ]]; then
   # notify-send "already open"
   exit
fi
if [[ $(hostname) == lappy9000 ]]; then
    ssh compy64 
    exit
fi
if [[ -z $(pgrep mpd) ]]; then
	  mpd
else
	  echo "found mpd"
fi
ncmpcpp
