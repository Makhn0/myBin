#!/usr/bin/env sh

arg_filter(){
	for i in $@;
		if [[ $i == -p ]] || [[ $i == -l ]]; then
			shift
			shift
		else
			printf "$i"
		fi

}
echo $@
arg_filter $@
[[ $(tty) != "not a tty" ]] && (fzf $(arg_filter $@) ; exit)
[[ $WAYLAND_SOMETHING ]] && (bmenu $@ ;exit)
dmenu $@
