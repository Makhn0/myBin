#!/bin/sh

echo ZWRAP here >> /dev/stderr
echo I see: >> /dev/stderr
echo "$@" >> /dev/stderr
RECENT_FILE=$HOME/.config/zathura/recentf
[ -z "$@" ] && echo gimme a bookname && exit 0
if [ "$1" = "-l" ]; then
      tac $RECENT_FILE
      exit 0
fi
if [ "$1" = "edit" ]; then
      vim $RECENT_FILE
      exit 0
fi
		      
if [[ "$@" =~ ^/.* ]]; then
    echo foo >> /dev/stderr
   FILEPATH="$@"
   else
       echo boo >> /dev/stderr
   FILEPATH="$(pwd)/$@"

fi
echo book=$FILEPATH >> /dev/stderr
sed -i "\\|$FILEPATH|d"  $RECENT_FILE
echo "$FILEPATH" >>  $RECENT_FILE
zathura "$FILEPATH"
