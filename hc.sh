#!/usr/bin/zsh


#

HYPRSOCKET2=$XDG_RUNTIME_DIR/hypr/$HYPRLAND_INSTANCE_SIGNATURE/.socket2.sock
BLACK=$HOME/pix/bg/.wallpapers/.black
PCMAN_ID=$(hyprctl clients |\
	       awk '/^Window .* -> .*:$/ {match($0, "Window .* ->",winary ) ;
				     window=substr(winary[0], 8,length(winary[0]) - 10)}
	       /workspace:/ {ws=substr($3,2,length($3)-2)}
	       /class: pcmanfm/  {if (ws~"special:pcmanfm")  {print window}}')
monitor0_orientation () {
    
    echo $(hyprctl monitors | awk '/[ \t]*transform: / {print $2}' |head -1)
    }
MONITOR_ORIENTATION=$(monitor0_orientation)
SPECIAL_WS_ACTIVE=false
handle () {
    #this will change the wallpaper on
    if [[ ${1:0:9} = "workspace" ]] && [[ ! -e  $BLACK ]]; then
	wallpaper reload 2>/dev/null >> /dev/null
	#notify-send "foo"
    elif [[ ${1:0:10} = "openwindow" ]]; then
	echo $1
	if [[ $(echo $1 | cut -d',' -f 2 ) = "special:pcmanfm" ]]; then
	    if [[ ! $(echo $1 | cut -d',' -f 3) = "pcmanfm" ]]; then
	    eww close launcher
		ws=$(hyprctl activeworkspace | head -1 | cut -d' ' -f 4| sed 's/(\|)//g')
		windowid=$(echo $1 | cut -d',' -f 1| cut -d'>' -f 3)
		echo ws,windowid=$ws,$windowid
		hyprctl dispatch movetoworkspace $ws,address:0x$windowid 
	    fi
	 fi
    elif [[ $1 =~ "activelayout>>video" ]] && [[ $MONITOR_ORIENTATION != $(monitor0_orientation) ]] ;then
	
             MONITOR_ORIENTATION=$(monitor0_orientation)
	     #echo $MONITOR_ORIENTATION
	     id=$(hyprctl activewindow | head -1 |sed 's/Window \([[:alnum:]]*\).*$/\1/')
	     #echo id=$id
#	     eww reload
	     #echo pcmanid=$PCMAN_ID
	     hyprctl dispatch movewindowpixel exact 5% 15%,address:0x$PCMAN_ID
	     hyprctl dispatch resizewindowpixel exact 92% 80%,address:0x$PCMAN_ID

	     if [[ $id = $PCMAN_ID ]]; then
		 #eww open --toggle launcher-landscape
		 #eww open --toggle launcher-portrait
		 #hyprctl dispatch centerwindow
		 #hyprctl resizeactive 
		 fi


	    #notify-send "rotated $(monitor0_orientation)"
	fi
    }
getaddress()
{
    #gets id of first window with given property
    awk_string="
      /Window/{address=\$2}
      /$1/ { print address; exit }
"
}
gettrash()
{
    # gets current window if it is in -99
    # if not gets last window in -99 if it exists
    # if not searches through all clients for first that has ws -99
    # returns nothing if no matches
a=0x$(hyprctl activewindow|awk ' BEGIN {ws=0;addr=0}
                               /^Window/ {addr=$2}
                              /workspace:/ {ws=$2}
                               ws==-99 {print addr;exit}
') ; echo gettrash activewindow method=$a &>/dev/stderr


if [[ $a =~ 0x[[:alnum:]]{8} ]];then
   echo $a
   exit
else
    b=$(hyprctl workspaces | awk ' BEGIN {m=0}
                 /^workspace ID.*\(special:trash\)/ {m=1}
                 /^[\t ]*lastwindow:/ && m==1 {print $2;exit}
')
fi
echo gettrash ws method= $b &>/dev/stderr
if [[ $b =~ 0x[[:alnum:]]{8} ]]; then
    echo $b
    exit
   else
    c=0x$(hyprctl clients | awk '/^Window/{addr=$2}
                                             /workspace:/ {ws=$2}
                               ws==-99 {print addr;exit}
')
echo $c
   fi
 echo gettrash clients method=$c &>/dev/stderr
}
getid()
{
    #gets id of first window with given property
hyprctl clients | \
awk "BEGIN{m=0;pid=\"\"}
      /Window/{pid=\"\"}

      /$1/{
         if( pid != \"\" )
           {print pid; exit}
          m=1}
     /pid:.*/ && m==1 {print \$2; exit}
     /pid:.*/ && m==0 {pid = \$2}
"
}
current_ws(){
    #takes argument and gets ws from
    #hc monitors by default (gets ws even if there is no active window,is never special)
    #hc ws if $1 is defined (gets only if active window exists, is sometimes special)
    #
    #if

   [ ! $1 ] &&
    hyprctl monitors | \
awk '
     /active workspace:/ {ws=$3}
     /focused/ && $2 == "yes" { print ws ; exit}
' && exit

    hyprctl activewindow | \
        awk '/workspace:/{print $2}'

    }

pull(){
    #echo arg=$1
    #echo id="$(getid "${1}")"
    currentws=$(current_ws)
    echo currentws=$currentws
    address="$(getaddress "${1}")"
    echo address="$address"
    #hyprctl dispatch movetoworkspace "$(current_ws),$(getid \"$1\")"
    echo "$currentws,0x$address"
    hyprctl dispatch movetoworkspace "$currentws,address:0x$address"
    }

trash_n()
{
    hyprctl workspaces | awk ' /workspace ID -99/{t=1}
                         /windows:/ && t==1 {print $2;exit}'


    }
pastetrash()
{
    #echo id="$(getid "${1}")"
    currentws=$(current_ws)
    echo currentws=$currentws
    realcurrentws=$(current_ws inc)
    echo realcurrentws=$realcurrentws
    address="$(gettrash)"
    echo address="<$address>"
    echo "$currentws,$address"
    trashN=$(trash_n)
    echo trashN=$trashN

    if [[ $(trash_n) != 1 ]] && [[  $realcurrentws == -99 ]];then
    Silent=silent
    fi

    echo dispatch=movetoworkspace$Silent "$currentws,:address:$address"
    [[ $1 != -d ]] && hyprctl dispatch movetoworkspace$Silent "$currentws,address:$address"
    }


toggleAnimations(){
    hyprctl keyword animations:enabled $(( ($(hyprctl getoption animations:enabled | awk '/int:/{print $2}') + 1 )% 2 ))
    }

searchclients(){
hyprctl clients | awk " /$1/ {a=1}
                        a==1 {print}
                        /^$/ {a=0}"
}
start_handler () {
socat - "UNIX-CONNECT:$HYPRSOCKET2" | \
	    while read -r line; 
	    do handle "$line";
	    done
}


case $1 in
  -p) pastetrash ;;
  -a) toggleAnimations ;;
  -c) shift && searchclients $@ ;;
  -s) start_handler ;;
  -t) echo $PCMAN_ID
esac && echo All ok || echo something now worky \($?\)
#pull "${1-workspace:.*special:trash}"
