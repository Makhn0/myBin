#!/bin/sh
source $HOME/bin/.mount-igor-stuff

mount_it ()
{
 	 sshfs -p $PORT $USER@$IP:$REMOTEDIR $MOUNTPOINT  $OPTIONS 

}

if [ "$1" = "-e" ];then 
	source ~/bin/.mount-e-stuff

	shift
fi
case $1 in
	-d) sudo umount -v $MOUNTPOINT ;;
	-c|-l) ls $MOUNTPOINT ;;
 	 -u|*) mount_it ;;
esac

