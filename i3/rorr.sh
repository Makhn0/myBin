#!/bin/bash
# raise or run script for i3wm

# works for
# [ "$(i3-msg '[class="Firefox"] focus')" = "[{\"success\":true}]" ] || i3-msg exec "firejail firefox"

echo ${1}
cmd="i3-msg '[class=${1}] focus'"
echo cmd= ${cmd}
i3out=$("$cmd")
echo i3out = "${i3out}"
# [ $i3out = "[{\"success\":true}]" ] || i3-msg exec "firejail firefox"
