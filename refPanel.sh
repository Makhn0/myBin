#!/bin/sh
pstree -lp | sed -rn "s/.*panel.*sleep\(([0-9]+)\).*/\1/p"  \
         | xargs kill && exit
if [[ $1 == "-k" ]]; then
    killall panel
    panel & disown
fi
