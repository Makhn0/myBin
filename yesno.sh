#!/bin/bash
#prompt user with yes or no, send answer to standard output or starts program
# a=$(echo -e "No\nYes" | myrofi -dmenu \
#                                -width 30 \
#                                -lines 2  \
                               # -i -p "$1")
a=$(echo -e "No\nYes" | dmenu -l 2 -p "$1")
shift
if [[ $1 ]]; then
    [[ $a == "Yes" ]]  &&  $1
else
    echo $a
fi

