#!/bin/bash
ram_n="$(bc <<< "scale=1; $(free | sed -n '2,2p' | awk '{ print $3 }')*100 / $(free | sed -n '2,2p' | awk '{ print $2 }')")"
# ram_n=77.8
if (( $(echo "$ram_n > 30" | bc -l) ));
then
    notify-send "ram usage highish"
    ram="RAM ${ram_n}% "
fi

swap="$(bc <<< "scale=1; $(free | sed -n '3,3p' | awk '{ print $3 }')*100 / $(free | sed -n '3,3p' | awk '{ print $2 }')")"
        # && [[ ! $a -eq 0 ]] && echo " $a"
swap_file=$HOME/.swap_use
if  (( $(echo "$swap > 0 " | bc -l) )); then
    if [[ ! -f $swap_file ]]; then
        notify-send "aww shit swap is being used ${swap}%"
        touch $swap_file
    fi
      swap="SWAP ${swap}%" 
else
    [ -f $swap_file ] && rm $swap_file
    unset swap
fi
printf "%s" "${ram}${swap}"
