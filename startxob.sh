#!/bin/sh
#
killall xob
[ -p /tmp/xobpipe ] || mkfifo /tmp/xobpipe
pidof xob || tail -f /tmp/xobpipe  | xob -c .config/xob/styles.cfg -s bw-$(hostname) & disown
