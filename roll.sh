#!/bin/bash

roll(){
	#roll N number of dm dice with modifier b
	n=$1
	m=$2
	b=$3
echo total= roll + mod
for i in $(seq $n)
do
	r=$(( (( $RANDOM+1) % $m +1 )  ))
	if [ $type = default ]; then
	echo   $r
	else
	echo $(( $r + $b )) = $r + $b
	fi

done |sort -hr

	}

n=${1-1}
m=${2-20}

if [[ $1 =~ "[0-9]+d[0-9]+" ]]; then
n=$(echo $string | sed 's/^\([0-9]*\)d\([0-9]*\)/\1/')
m=$(echo $string | sed 's/^\([0-9]*\)d\([0-9]*\)/\2/')

fi

shift
shift
temp=$1
type=${1-default}
case $type in
	str) b=1 ;;
	dex)   b=4 ;; # is also initiative
	const) b=-1;;
	int) b=4 ;;
	wis) b=2 ;;
	char) b=3 ;;
	acro) b=4;;
	animal) b=2;;
	arcana) b=4;;
	athletics) b=1;;
	dec) b=5;;
	his) b=4;;
	insight) b=2;;
	inv) b=8;;
	med) b=2;;
	nat) b=4;;
	perc) b=4;;
	perf) b=4;;
	rel) b=4;;
	sleight) b=4;;
	stealth) b=4;;
	survival) b=4;;
	*) b=$temp;;
esac



#echo $n $m $b
roll $n $m $b
