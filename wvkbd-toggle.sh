#!/bin/sh
NAME=wvkbd-canary
if pgrep $NAME ; then
	killall -9 $NAME
else
	$NAME -L 310 -H 430
fi
